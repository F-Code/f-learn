package fcode.controller;

import fcode.common.PaginationCommon;
import fcode.json.MemberForgotPassword;
import fcode.json.MemberLogin;
import fcode.model.Member;
import fcode.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class MemberController {

    @Autowired
    private MemberService memberService;

    // Lấy toàn bộ danh sách Member
    @RequestMapping(value = "/members", method = RequestMethod.GET)
    public ResponseEntity<List<Member>> getListMember() {
        List<Member> members = this.memberService.getListMember();
        return new ResponseEntity<>(members, HttpStatus.OK);
    }

    // Lấy danh sách Member theo page trang
    @RequestMapping(value = "/members/page/{page}", method = RequestMethod.GET)
    public ResponseEntity<List<Member>> getListPaginationMember(@PathVariable("page") int page) {
        List<Member> members = this.memberService.getListMember();
        int totalRecord = members.size();
        //Vị trí phần tử đầu tiên và cuối cùng của mảng khi phân trang
        int start = 0, end = 0;
        if (totalRecord > PaginationCommon.LIMIT_NUMBER_IN_PAGE) {
            // Vị trí phần tử đầu tiên của trang
            start = (page - 1) * PaginationCommon.LIMIT_NUMBER_IN_PAGE;

            // Vị trí phần tử cuối cùng của trang
            end = (end > totalRecord) ? (page * PaginationCommon.LIMIT_NUMBER_IN_PAGE) : totalRecord;

            //Exception khi page nhập vào < 1 hoặc vị trí của phần tử > tổng số bản ghi hiện có
            if (page < 1 || start > totalRecord) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            members = members.subList(start, end);
        }
        return new ResponseEntity<>(members, HttpStatus.OK);
    }

    // Lấy thông tin theo id
    @RequestMapping(value = "/member/{id}", method = RequestMethod.GET)
    public ResponseEntity<Member> getMemberById(@PathVariable("id") String id) {
        Member member = this.memberService.getMemberById(id);
        if (member != null) {
            return new ResponseEntity<>(member, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // Tạo mới 1 thành viên
    @RequestMapping(value = "/member", method = RequestMethod.POST)
    public ResponseEntity<Member> createMember(@RequestBody Member member) {
        Member byDisplayName = this.memberService.findByDisplayName(member.getDisplayName());
        Member byEmailAddress = this.memberService.findByEmailAddress(member.getDisplayName());

        if (byDisplayName != null) {
            return new ResponseEntity<>(byDisplayName, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (byEmailAddress != null) {
            return new ResponseEntity<>(byEmailAddress, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        this.memberService.createMember(member);
        return new ResponseEntity<>(member, HttpStatus.CREATED);

    }

    // Sửa thông tin thành viên
    @RequestMapping(value = "/member/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Member> editMember(@PathVariable("id") String id, @RequestBody Member member) {
        Member getMember = this.memberService.getMemberById(id);
        if (getMember != null) {
            this.memberService.editMember(member);
            return new ResponseEntity<>(member, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    // Xóa thành viên
    @RequestMapping(value = "/member/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteMember(@PathVariable("id") String id) {
        Member member = this.memberService.getMemberById(id);
        if (member != null) {
            this.memberService.deleteMember(id);
            return new ResponseEntity<>(member.getDisplayName(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // Đăng nhập với role member
    @RequestMapping(value = "/member/login", method = RequestMethod.POST)
    public ResponseEntity<Member> loginMember(@RequestBody MemberLogin memberLogin) {
        String displayNameOrEmailAddress = memberLogin.getDisplayNameOrEmailAddress();
        String password = memberLogin.getPassword();

        Member member = this.memberService.login(displayNameOrEmailAddress, password);
        if (member != null) {
            return new ResponseEntity<>(member, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // Đăng nhập với role member
    @RequestMapping(value = "/member/forgot-password", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> forgotPassword(@RequestBody MemberForgotPassword memberForgotPassword) {
        String email = memberForgotPassword.getEmailAddress();
        String password = memberForgotPassword.getPassword();
        String repassword = memberForgotPassword.getRepassword();
        Boolean isChange = this.memberService.forgotPassword(email, password, repassword);
        if (isChange) {
            return new ResponseEntity<>(isChange, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
