package fcode.controller;

import fcode.common.PaginationCommon;
import fcode.model.Confesstion;
import fcode.service.ConfesstionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class ConfesstionController {

    @Autowired
    private ConfesstionService confesstionService;

    // Lấy toàn bộ danh sách Confesstion
    @RequestMapping(value = "/confesstions", method = RequestMethod.GET)
    public ResponseEntity<List<Confesstion>> getListConfesstion() {
        List<Confesstion> confesstions = this.confesstionService.getListConfesstion();
        return new ResponseEntity<>(confesstions, HttpStatus.OK);
    }

    // Lấy danh sách Confesstion theo page trang
    @RequestMapping(value = "/confesstions/page/{page}", method = RequestMethod.GET)
    public ResponseEntity<List<Confesstion>> getListPaginationConfesstion(@PathVariable("page") int page) {
        List<Confesstion> confesstions = this.confesstionService.getListConfesstion();
        int totalRecord = confesstions.size();
        //Vị trí phần tử đầu tiên và cuối cùng của mảng khi phân trang
        int start = 0, end = 0;
        if (totalRecord > PaginationCommon.LIMIT_NUMBER_IN_PAGE) {
            // Vị trí phần tử đầu tiên của trang
            start = (page - 1) * PaginationCommon.LIMIT_NUMBER_IN_PAGE;

            // Vị trí phần tử cuối cùng của trang
            end = (end > totalRecord) ? (page * PaginationCommon.LIMIT_NUMBER_IN_PAGE) : totalRecord;

            //Exception khi page nhập vào < 1 hoặc vị trí của phần tử > tổng số bản ghi hiện có
            if (page < 1 || start > totalRecord) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            confesstions = confesstions.subList(start, end);
        }
        return new ResponseEntity<>(confesstions, HttpStatus.OK);
    }

    // Lấy thông tin theo id
    @RequestMapping(value = "/confesstion/{id}", method = RequestMethod.GET)
    public ResponseEntity<Confesstion> getConfesstionById(@PathVariable("id") String id) {
        Confesstion confesstion = this.confesstionService.getConfesstionById(id);
        if (confesstion != null) {
            return new ResponseEntity<>(confesstion, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // Tạo mới 1 thành viên
    @RequestMapping(value = "/confesstion", method = RequestMethod.POST)
    public ResponseEntity<Confesstion> createConfesstion(@RequestBody Confesstion confesstion) {
        this.confesstionService.createConfesstion(confesstion);
        return new ResponseEntity<>(confesstion, HttpStatus.CREATED);

    }

}
