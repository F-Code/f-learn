package fcode.service;

import fcode.model.Member;

import java.util.List;

public interface MemberService {

    /**
     * Lấy danh sách thành viên
     * @return List<Member>
     */
    List<Member> getListMember();

    /**
     * Lấy thông tin member theo id
     * @param id
     * @return Member
     */
    Member getMemberById(String id);

    /**
     * Thêm mới 1 member
     * @param member
     * @return Member
     */
    Member createMember(Member member);

    /**
     * Sửa thông tin member
     * @param member
     * @return Member
     */
    Member editMember(Member member);

    /**
     * Xóa 1 member
     * @param id
     */
    void deleteMember(String id);

    /**
     * Login với tên định danh hoặc email
     * @param displayNameOrEmailAddress
     * @param password
     * @return Member
     */
    Member login(String displayNameOrEmailAddress, String password);

    /**
     * Tìm thành viên theo tên hiển thị
     * @param displayName
     * @return Member
     */
    Member findByDisplayName(String displayName);

    /**
     * Tìm thành viên theo email
     * @param emailAddress
     * @return Member
     */
    Member findByEmailAddress(String emailAddress);

    /**
     * Quên mật khẩu
     * @param emailAddress
     * @param password
     * @param repassword
     * @return boolean
     */
    boolean forgotPassword(String emailAddress, String password, String repassword);

}
