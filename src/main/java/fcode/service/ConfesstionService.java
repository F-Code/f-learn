package fcode.service;

import fcode.model.Confesstion;

import java.util.List;

public interface ConfesstionService {
    /**
     * Lấy danh sách thành viên
     *
     * @return List<Confesstion>
     */
    List<Confesstion> getListConfesstion();

    /**
     * Lấy thông tin confesstion theo id
     *
     * @param id
     * @return Confesstion
     */
    Confesstion getConfesstionById(String id);

    /**
     * Thêm mới 1 confesstion
     *
     * @param confesstion
     * @return Confesstion
     */
    Confesstion createConfesstion(Confesstion confesstion);
}
