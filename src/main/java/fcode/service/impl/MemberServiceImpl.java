package fcode.service.impl;

import com.mongodb.MongoClient;
import fcode.common.DatabaseCommon;
import fcode.model.Member;
import fcode.repository.MemberRepository;
import fcode.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.apache.log4j.Logger;

import java.util.List;

@Service
@Transactional
public class MemberServiceImpl implements MemberService {

    private static final Logger LOGGER = Logger.getLogger(MemberServiceImpl.class);

    @Autowired
    private MemberRepository memberRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public List<Member> getListMember() {
        LOGGER.info("getListMember");
        return this.memberRepository.findAll();
    }

    @Override
    public Member getMemberById(String id) {
        LOGGER.info("getMemberById");
        return this.memberRepository.findOne(id);
    }

    @Override
    public Member createMember(Member member) {
        LOGGER.info("BCryptPasswordEncoder Password");
        member.setPassword(passwordEncoder.encode(member.getPassword()));
        LOGGER.info("createMember");
        return this.memberRepository.save(member);
    }

    @Override
    public Member editMember(Member member) {
        LOGGER.info("editMember");
        return this.memberRepository.save(member);
    }

    @Override
    public void deleteMember(String id) {
        this.memberRepository.delete(id);
    }

    @Override
    public Member login(String displayNameOrEmailAddress, String password) {
        Member displayNameExists = findByDisplayName(displayNameOrEmailAddress);
        Member emailAddressExists = findByEmailAddress(displayNameOrEmailAddress);

        // Nếu tên hiển thị khác null
        if (displayNameExists != null) {
            if (!passwordEncoder.matches(password, displayNameExists.getPassword())) {
                LOGGER.info("Incorrect password");
                return null;
            }
        }

        // Nếu email khác null
        if (emailAddressExists != null) {
            if (!passwordEncoder.matches(password, emailAddressExists.getPassword())) {
                LOGGER.info("Incorrect password");
                return null;
            }
        }

        // Cả email cả tên hiển thị null
        if (displayNameExists == null && emailAddressExists == null) {
            LOGGER.info("Not found Member");
            return null;
        }

        return displayNameExists != null ? displayNameExists : emailAddressExists;
    }

    @Override
    public Member findByDisplayName(String displayName) {
        Member displayNameExists = memberRepository.findByDisplayName(displayName);
        return displayNameExists != null ? displayNameExists : null;
    }

    @Override
    public Member findByEmailAddress(String emailAddress) {
        Member emailAddressMember = memberRepository.findByEmailAddress(emailAddress);
        return emailAddressMember != null ? emailAddressMember : null;
    }

    @Override
    public boolean forgotPassword(String emailAddress, String password, String repassword) {
        if (findByEmailAddress(emailAddress) == null) {
            return false;
        }
        if (!password.equals(repassword)) {
            return false;
        }
        try {
            MongoClient mongo = new MongoClient(DatabaseCommon.TYPE_OF_SERVER);
            Query query = new Query(Criteria.where("emailAddress").is(emailAddress));
            Update update = new Update();
            update.set("password", passwordEncoder.encode(password));
            MongoTemplate mongoTemplate = new MongoTemplate(mongo, DatabaseCommon.NAME_DATABASE);
            mongoTemplate.updateFirst(query, update, Member.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

}
