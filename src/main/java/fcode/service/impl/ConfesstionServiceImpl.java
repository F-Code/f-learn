package fcode.service.impl;

import fcode.model.Confesstion;
import fcode.repository.ConfesstionRepository;
import fcode.service.ConfesstionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConfesstionServiceImpl implements ConfesstionService {

    private static final Logger LOGGER = Logger.getLogger(MemberServiceImpl.class);

    @Autowired
    private ConfesstionRepository confesstionRepository;

    @Override
    public List<Confesstion> getListConfesstion() {
        LOGGER.info("getListConfesstion");
        return this.confesstionRepository.findAll();
    }

    @Override
    public Confesstion getConfesstionById(String id) {
        LOGGER.info("getConfesstionById");
        return this.confesstionRepository.findOne(id);
    }

    @Override
    public Confesstion createConfesstion(Confesstion confesstion) {
        LOGGER.info("createConfesstion");
        return this.confesstionRepository.save(confesstion);
    }
}
