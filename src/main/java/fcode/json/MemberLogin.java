package fcode.json;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//Dùng để check login
@Data
public class MemberLogin {

    //Tên hiển thị
    private String displayNameOrEmailAddress;

    // Mật khẩu
    private String password;

}
