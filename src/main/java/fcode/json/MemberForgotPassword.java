package fcode.json;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//Dùng để check login
@Data
public class MemberForgotPassword {

    //Email đăng nhập
    private String emailAddress;

    // Mật khẩu
    private String password;

    // Nhập lại mật khẩu
    private String repassword;

}
