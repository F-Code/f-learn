package fcode.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// Khóa học
@Document(collection = "course")
@Data
public class Course {

    // ID MongoDB
    @Id
    private String id;

    //Tên hiển thị
    private String displayName;

    // Họ tên đầy đủ
    private String familyName;

    //Email đăng nhập
    private String emailAddress;

    // Mật khẩu
    private String password;

    //Ngày sinh
    private String birthYear;

    //Số điện thoại
    private String phoneNumber;

    // Tỉnh
    private String province;

    // Thông tin đường
    private String streetInformation;

    // Giới thiệu bản thân
    private String summary;

    // Ảnh đại diện
    private String avatar;

    @Override
    public String toString() {
        return "Member{" +
                "id='" + id + '\'' +
                ", displayName='" + displayName + '\'' +
                ", familyName='" + familyName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", password='" + password + '\'' +
                ", birthYear='" + birthYear + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", provinceId=" + province +
                ", streetInformation='" + streetInformation + '\'' +
                ", summary='" + summary + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
