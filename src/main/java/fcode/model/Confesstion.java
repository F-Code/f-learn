package fcode.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// Thành viên CLB
@Document(collection = "confesstion")
@Data
public class Confesstion {

    // ID MongoDB
    @Id
    private String id;

    //Nội dung cần chia sẻ
    private String content;

    /**
     * Đã được đăng lên FB : 0 - 1
     */
    private int status;

}
