package fcode.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user")
@Data
public class User {

    // ID MongoDB
    @Id
    private String id;

    //Tên hiển thị
    private String displayName;

    // Họ tên đầy đủ
    private String familyName;

    //Email đăng nhập
    private String emailAddress;

    // Mật khẩu
    private String password;

    //Ngày sinh
    private String birthYear;

    //Số điện thoại
    private String phoneNumber;

    // Mã tỉnh
    private int provinceId;

    // Mã huyện
    private int districtId;

    // Thông tin đường
    private String streetInformation;

    // Giới thiệu bản thân
    private String summary;

    // Ảnh đại diện
    private String avatar;

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", displayName='" + displayName + '\'' +
                ", familyName='" + familyName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", password='" + password + '\'' +
                ", birthYear='" + birthYear + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", provinceId=" + provinceId +
                ", districtId=" + districtId +
                ", streetInformation='" + streetInformation + '\'' +
                ", summary='" + summary + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
