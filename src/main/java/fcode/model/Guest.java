package fcode.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// Khách tham gia F-Share
@Document(collection = "guest")
@Data
public class Guest {

    // ID MongoDB
    @Id
    private String id;

    // Họ tên đầy đủ
    private String familyName;

    //Email đăng nhập
    private String emailAddress;

    // Mật khẩu
    private String password;

    //Số điện thoại
    private String phoneNumber;

    // Ảnh đại diện
    private String avatar;

    @Override
    public String toString() {
        return "Member{" +
                "id='" + id + '\'' +
                ", familyName='" + familyName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", password='" + password + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
