package fcode.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

// Thành viên CLB
@Document(collection = "member")
@Data
public class Member {

    // ID MongoDB
    @Id
    private String id;

    //Tên hiển thị
    private String displayName;

    // Họ tên đầy đủ
    private String familyName;

    //Email đăng nhập
    private String emailAddress;

    // Mật khẩu
    private String password;

    //Ngày sinh
    private String birthYear;

    //Số điện thoại
    private String phoneNumber;

    // Tỉnh
    private String province;

    // Thông tin đường
    private String streetInformation;

    // Chức vụ
    private String position;

    // Giới thiệu bản thân
    private String summary;

    // Ảnh đại diện
    private String avatar;

    // Tên trường học
    private String school;

    // Chuyên ngành
    private String department;

    // Khóa học example: 12.3
    private String grade;

    @Override
    public String toString() {
        return "Member{" +
                "id='" + id + '\'' +
                ", displayName='" + displayName + '\'' +
                ", familyName='" + familyName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", password='" + password + '\'' +
                ", birthYear='" + birthYear + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", province='" + province + '\'' +
                ", streetInformation='" + streetInformation + '\'' +
                ", position='" + position + '\'' +
                ", summary='" + summary + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
