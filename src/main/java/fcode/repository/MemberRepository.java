package fcode.repository;

import fcode.model.Member;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MemberRepository extends MongoRepository<Member, String> {
    Member findByDisplayName(String displayName);

    Member findByEmailAddress(String emailAddress);

}
